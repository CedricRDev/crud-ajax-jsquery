<?php


class Ticket{

    private int $_id;
    private string $_nom;
    private string $_prenom;
    private string $_ddn;
    private string $_adresse;
    private string $_mail;
    private string $_telephone;

    public function __consruct(int $id, string $nom, string $prenom, string $ddn, string $adresse, string $mail, string $telephone){
        $this->id = $id;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->ddn = $ddn;
        $this->adresse = $adresse;
        $this->mail = $mail;
        $this->telephone = $telephone;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getNom(): string
    {
        return $this->_nom;
    }
    public function setNom(string $nom): self
    {
        $this->_nom = $nom;
        return $this;
    }
    public function getPrenom(): string
    {
        return $this->_prenom;
    }
    public function setPrenom(string $prenom): self
    {
        $this->_prenom = $prenom;
        return $this;
    }
    public function getDdn(): string
    {
        return $this->_ddn;
    }
    public function setDdn(string $ddn): self
    {
        $this->_ddn = $ddn;
        return $this;
    }
    public function getAdresse(): string
    {
        return $this->_adresse;
    }
    public function setAdresse(string $adresse): self
    {
        $this->_adresse = $adresse;
        return $this;
    }
    public function getMail(): string
    {
        return $this->_mail;
    }
    public function setMail(string $mail): self
    {
        $this->_mail = $mail;
        return $this;
    }
    public function getTelephone(): string
    {
        return $this->_telephone;
    }
    public function setTelephone(string $telephone): self
    {
        $this->_telephone = $telephone;
        return $this;
    }
}
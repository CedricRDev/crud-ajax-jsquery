$(document).ready(function () {
    sendTicket();
    getTickets();
    deleteTicket();
    getOneTicket();
})

function sendTicket() {
    $(document).on('click', '#bouton-ajout', function () {
        let nom = $('#nom').val();
        let prenom = $('#prenom').val()
        let ddn = $('#ddn').val()
        let adresse = $('#adresse').val()
        let mail = $('#email').val()
        let telephone = $('#tel').val()
        console.log(telephone)

        // On vérifie que tous les champs soient remplies:
        if (nom == "" || prenom == "" || ddn == "" || adresse == '' || mail == "" || telephone == "") {
            $('#messageTicket').html('Veuillez remplir tous les champs vides')
                                
            
        
        }
        if($.isNumeric( $('tel').val()) ){

                $('#messageTicket').html('yes');
            }
        else {
            $.ajax({
                method: 'post',
                url: 'insertTicket.php',
                data: {
                    Nom: nom,
                    Prenom: prenom,
                    Ddn: ddn,
                    Adresse: adresse,
                    Mail: mail,
                    Telephone: telephone,
                },
                success: function (data) {
                    $('#message').html('Votre ticket à bien été sauvegardé');
                     $('#NewTicket').modal('show');
                    getTickets()
                },
                error: function(){
                    $('messageTicket').html('Cet email existe déjà');
                    $('#NewTicket').modal('show');
                }
            })
        }
    });
}

function getTickets() {
    $.ajax({
        type: 'post',
        url: 'getTickets.php',
        success: function (data) {
            console.log(data)

            data = $.parseJSON(data);
            if (data.status == 'success') {
                $('#table').html(data.html)
            }
        }
    });
}

function deleteTicket() {
    $(document).on('click', '#delete-ticket', function () {

        let id = +$(this).attr('ticketId');
        /*   console.log(id)*/
        if (!id) {
            $('#message').html('Problème lors de la suppression');
        }
        else {
            $.ajax({
                method: 'post',
                url: 'deleteTicket.php',
                data: {
                    IdTicket: id
                },
                success: function (data) {
                    $('#message').html('Le ticket a été supprimé');

                    getTickets();
                }
            })
        }
    })
}

function getOneTicket() {
    $(document).on('dblclick', '#card-ticket', function () {

        let id = +$(this).attr('ticketId');

        if (!id) {
            $('#message').html('Problème pour récupérer le ticket');
        }
        else {
            $.ajax({
                method: "post",
                url: 'getOneTicket.php',
                data: {
                    IdTicket: id
                },
                success: function (data) {
                    data = $.parseJSON(data);
                    if (data.status == 'success') {
                        $('#detail-ticket').html(data.html)
                   console.log(data); }
                }
                
            })
        }
   /*  $('#card-ticket').appendTo('#thumb-ticket') */

    })
}

function hide() {
    
}

function show() {
    
}
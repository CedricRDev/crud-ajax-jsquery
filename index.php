<!DOCTYPE html>

<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    
    <link rel='stylesheet' href='css/bootstrap.css'>
    <link rel='stylesheet' href='css/main2.css'>
    <script src='js/jquery.js'></script>
    <script src='js/bootstrap.js'></script>
    <script src='js/ajax.js'></script>
    <script src='js/dateHeure.js'></script>
    <script type="text/javascript"> </script>

    <title>Test Développeur Web T2iTélécom</title>
</head>

<body>

    <!-- Affichage de l'heure -->
    

    <!-- Modal :  Superviseur Général -->
    <div id="ticket-container">
        <h3 class="display-4">Alarmes Libres</h3>
        <div id="date">
                <script>
                    afficherDate();
                </script>
        </div>
        <div class="row">
            <div class="col">
                <div class="card mt-5">
                    <div class="card-title ml-5 my-2">
                        <!-- Boutton new ticket -->
                        <button type="button" class="button-new btn btn-primary" onclick="displayForm()" id="btn-new-ticket" data-bs-toggle="modal" data-bs-target="#new-ticket">New ticket</button>
                        
                    </div>
                    <p id="message" class='text-dark'></p>
                    <div class="card-body">
                        <div id="table"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal new ticket -->
        <div  id="new-ticket" class="new-ticket modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="newTicketLabel" aria-hidden="true">
             <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header"> 
                        <h3 class="modal-title" id="newTicketLabel">Nouveau ticket</h3>
                        <button type="button-new" class="button-close btn btn-secondary" data-bs-dismiss="modal" aria-label="Close">X</button>
                    </div>

                    <div class="modal-ticket-body">
                    <p id="messageTicket" class='text-dark'></p>
                        <form>
                            <label for="prenom">Prénom</label>
                            <input id="prenom" name="prenom" type="text" class="form-control my-2">

                            <label for="nom">Nom</label>
                            <input id="nom" name="nom" type="text" class="form-control my-2">

                            <label for="ddn">ddn</label>
                            <input id="ddn" name="ddn" type="date" class="form-control my-2">

                            <label for="adresse">adresse</label>
                            <input id="adresse" name="adresse" type="text" class="form-control my-2">


                            <label for="email">email</label>
                            <input id="email" name="email" type="email" class="form-control my-2">

                            <label for="tel">Téléphone</label>
                            <input id="tel" name="tel" type="text" class="form-control my-2">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" onclick="closeForm()" id="bouton-ajout" class="button-success btn btn-success ">Enregistrer</button>
                        <button type="button" onclick="closeForm()" id="bouton-fermer" class="button-close btn btn-danger" data-bs-dismiss="modal">Fermer</button>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <!-- Modal :  Alarmes Prises -->
    <div>
        <h3 class="display-4">Alarmes prises</h3>

        <div id="thumb-ticket">

        </div>
    </div>

    <!-- Modal :  Affichages des infos -->

    <div>  
        <h3 class="display-4">Zone d'affichage des infos</h3>
            <div id="container-ticket">

                <div id="detail-ticket">
                 
                </div>
        </div>

        <div class="">
         
    </div>
    <script src='js/functions.js'></script>
</body>

</html>
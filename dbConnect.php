<?php

function dbConnect($dbname, $username, $password)
{
    try {
        $dbh = new PDO("mysql:host=localhost;dbname=" . $dbname . ";charset=utf8;", $username, $password);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        //Dans le cas ou la bdd n'est pas crée :
        $table = "ticket";     
        $sql="CREATE TABLE IF NOT EXISTS $table(
            id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
            nom varchar(255) NOT NULL ,
            prenom varchar(255) NOT NULL ,
            ddn Date NOT NULL ,
            adresse varchar(255)  NOT NULL  ,
            mail varchar(255) NOT NULL UNIQUE,
            telephone varchar(255) NOT NULL);" ;         
    
        $dbh->exec($sql);

    } catch (PDOException $exception) {
        die( "Erreur connexion à la base de données : ". $exception->getMessage() );
    }
    return $dbh;
}



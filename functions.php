<?php

require_once('dbConnect.php');
 
    /**
     * Fonction pour enregistrer un ticket en base de données
     * @return void
     */
function sendTicket(){
    require_once('classTicket.php');

       $prenom = $_POST["Prenom"];
       $nom = $_POST["Nom"];
       $ddn = $_POST["Ddn"];
       $adresse = $_POST["Adresse"];
       $mail = $_POST["Mail"];
       $tel = $_POST["Telephone"];       
           
        $dbh = dbConnect("test_t2itelecom", "root", "");
        $ticket = new Ticket($nom);
        $ticket->setNom($nom)
               ->setPrenom($prenom)
               ->setDdn($ddn)
               ->setAdresse($adresse)
               ->setMail($mail)
               ->setTelephone($tel);     
   
        $query = "INSERT INTO ticket ( nom, prenom, ddn, adresse, mail, telephone) VALUES (:nom, :prenom, :ddn, :adresse, :mail, :telephone)";      
        try{
            $stmt = $dbh -> prepare($query);       
            $stmt -> execute([
                ":nom" => $nom,
                ":prenom" => $prenom,
                ":ddn" => $ddn,
                ":adresse" => $adresse,
                ":mail"=> $mail,
                ":telephone" => $tel
            ]);
        }
        catch(Exception $e) {
                //En cas d'erreur :
                echo "Problème lors de la requète : " . $e->getMessage();            
        } 

        /* Test pour vérifier si la contrainte d'unicité sur un email a été déclenché. Si c'est le cas, le code d'état 23000 et le code d'erreur 1062 seront renvoyé dans errorInfo */
        if ($stmt->errorInfo()[0] === "23000" && $stmt->errorInfo()[1] === 1062) {           
            echo " L'adresse ". $mail . " existe déjà !";
        }      
}

    /**
    * Permet de récupérér tous les tickets
    * @return void
    */
function getTickets() : void
{
    $dbh = dbConnect("test_t2itelecom", "root", "");
  // var_dump($dbh->quote() );
    $valeur = "";
    $valeur ='<table class = "table">
                <div>  
                    <tr>                
                        <td> Nom </td>                                        
                                      
                    </tr>
                </div>';
  
    $query = "SELECT * from ticket";
    try{
        $stmt = $dbh->prepare($query);
        $stmt-> execute();
    }
    catch(Exception $e) {
        //En cas d'erreur :
        echo "Problème lors de la requète" . $e->getMessage();            
    }     

    $result = $stmt-> fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $row){
            // On récupère l'ID de chaque ticket
                $id = +$row["id"] ;

                $valeur .= '
                        <div > 
                            <tr>
                               
                                <td id="card-ticket" ticketId='.$id.' ondblclick="getOneTicket()">'. htmlspecialchars(ucfirst(strtolower($row["nom"]))).'                                 
                                 <input type="button" class = "button-close btn btn-danger" id = "delete-ticket" ticketId='.$id.' value="Supprimer"></td>
                            </tr>
                        </div>';
            
        }
    $valeur .= '</table>'; 
    echo json_encode(['status'=> 'success', 'html'=> $valeur]);
}

    /**
     * Supprime un ticket via son id
     * @return void
     */
function deleteTicket() : void {
    
    $dbh = dbConnect("test_t2itelecom", "root", "");

    if(!$_POST['IdTicket']){
        var_dump('Le n\'existe pas ou plus'); 
    }
    else{
        $IdTicket = +$_POST['IdTicket'];
    
        $deleteTicket = "DELETE FROM ticket WHERE id = ? ";
        $data= [$IdTicket];
        try{
            $stmt = $dbh -> prepare($deleteTicket);        
            $stmt -> execute($data);
        }
        catch(Exception $e) {
                //En cas d'erreur :
                echo "Problème lors de la requète" . $e->getMessage();            
        }
        getTickets();
    }
}

    /**
     * Récupère un ticket par son id
     * @return object|null
     */
function getOneTicket()/* : object| null  */
{
   
    $dbh = dbConnect("test_t2itelecom", "root", "");   
    $value = "";
   
    if(!$_POST['IdTicket']){
        var_dump('Le ticket n\'existe pas ou plus'); 
    }
    else{
        $IdTicket = +$_POST['IdTicket'];   

        $query = "SELECT * FROM ticket WHERE id = ?";
        $data = [$IdTicket];

        try{
            $stmt = $dbh -> prepare($query);        
            $stmt -> execute($data);
        }
        catch(Exception $e) {
                //En cas d'erreur :
                echo "Problème lors de la requète" . $e->getMessage();            
        }
        $result =  $stmt->fetchObject() ;

        $dateNow = new \DateTime('now');
        $ddn = new \DateTime($result->ddn);
            $YearNow = $dateNow->format('Y');      
            $age = $YearNow - $ddn->format('Y');
        $ddn->modify('this year');
        

        ;

        $thisYear = $dateNow->format('Y');
        $thisMonth = $ddn->format('m');
        $thisDay = $ddn->format('d');
   
         //on crée une date avec +7 jours par rapport à la ddn
        $newDdn = (new \DateTime())->setDate($thisYear, $thisMonth, $thisDay);  
       
        $interval = $dateNow->diff($newDdn)->format("%a");
        
        $num = intval($interval); // on converti la string en int
                   
                
            //Si la ddn correspond au mois en cours :
            if ( $ddn->format('m') == $dateNow->format('m') ){               
           
                //Si c'est le jour anniversaire :
                if( $ddn->format('d') == $dateNow->format('d') ){
                    $value .= '<p> Age : <span class="red">'.$age.' ans</span></p>';
                   
                }
                //Si c'est la semaine qui précède : 
                elseif($num >0 && $num <8 &&  $dateNow > $newDdn  )
                {                   
                    $value .= '<p> Age : <span class="orange">'.$age.' ans</span></p>';
                }  
                
                 //Si c'est la semaine suivante :
                elseif( $num > 0 && $num < 8 &&  $dateNow < $newDdn   ){
                    
                        $value .= '<p> Age : <span class="yellow">'.$age.' ans</span></p>';                        
                    
                }else {
                    $value .= '<p class=""> Age : '.$age.' ans</p>';
                } 
            }    
           
        $prenom =  htmlspecialchars(ucfirst($result->prenom));
        $nom = htmlspecialchars(ucfirst($result->nom));
        $ddn = htmlspecialchars($ddn->format('d-m-Y'));
        $adresse = htmlspecialchars(strtolower($result->adresse));
        $mail = htmlspecialchars(strtolower($result->mail));
        $telephone = htmlspecialchars($result->telephone);
       
      
        $value .= ' 
                    
                    <p> Prénom :  '.($prenom).' </p>
                    <p> Nom : '.($nom).' </p>
                    <p> Date de Naissance : '.$ddn.' </p>
                    <p> Adresse : '.($adresse).' </p>
                    <p> Email : '.($mail).' </p>
                    <p> Téléphone : '. $telephone.' </p>
                    ';            
        
        echo json_encode(['status'=> 'success', 'html'=> $value]);   
         /* return $result;  */
    
    }
}

    /**
     * Permet de calculer l'âge
     * @return int|null
     */
function calculAge(): int | null
{
    $age = 25;          
    
   /* $dateNow = new \DateTime();       
    $YearNow = $dateNow->format('Y');      
    $age = $YearNow - $ddnFR->format('Y');
     $dbh = dbConnect("test_t2itelecom", "root", "");

        $query = "SELECT * FROM ticket WHERE id = '123'";    

        try{
            $stmt = $dbh -> prepare($query);        
            $stmt -> execute($data);
        }
        catch(Exception $e) {
                //En cas d'erreur :
                echo "Problème lors de la requète" . $e->getMessage();            
        }

        $result = $stmt->fetchObject(); // on récupère le ticket en "objet"

        if($result){
            // On récupère la ddn et on la converti en format date
            $ddn = new \DateTime($result->ddn);           
    
            $dateNow = new \DateTime();       
            $YearNow = $dateNow->format('Y');      
            $age = $YearNow - $ddn->format('Y');
         }  */

        return $age;
}

